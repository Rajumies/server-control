import warnings
with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=SyntaxWarning)
    from server import Server
import R720ipmicredentials
import R720XDipmicredentials
import logging
import time
import sys
from ping_host import ping_host

logging.basicConfig(format='%(message)s', level=logging.INFO)

def startup_server(server):

    server.set_manual_fan_control()
    server.set_fan_speed(0x06)
    time.sleep(1)
    server.boot_server()

def poweroff_server(server):
    server.shutdown_server()

def startup_rack():
    logging.info("Starting " + R720XD.idrac_addr)
    startup_server(R720XD)
    logging.info("Waiting for hypervisor " + R720XD.hv_addr + " to be up")
    while not ping_host(R720XD.hv_addr):
        time.sleep(5)
        logging.info("Hypervisor not up yet")
    logging.info("Hypervisor " + R720XD.hv_addr + " Responded to ICMP ping")
    R720XD.set_fan_speed(0x0A)
    logging.info("Waiting 30 seconds to give some headroom")
    time.sleep(30)
    logging.info("Starting " + R720.idrac_addr)
    startup_server(R720)
    time.sleep(120)
    R720.set_automatic_fan_control()
    logging.info("Resumed automatic fan control on " + R720.idrac_addr)


def poweroff_rack():
    logging.info("Shutting down " + R720.idrac_addr)
    poweroff_server(R720)
    logging.info("Waiting for hypervisor " + R720.hv_addr + " to shut down")
    while ping_host(R720.hv_addr):
        time.sleep(5)
        logging.info("Hypervisor not yet down")
    logging.info("Hypervisor " + R720.hv_addr + " No longer responds to ICMP ping")
    logging.info("Shutting down " + R720XD.idrac_addr)
    poweroff_server(R720XD)


if __name__ == '__main__':
    R720 = Server(R720ipmicredentials.idrac,
                  R720ipmicredentials.hv,
                  R720ipmicredentials.username,
                  R720ipmicredentials.password)
    R720XD = Server(R720XDipmicredentials.idrac,
                    R720XDipmicredentials.hv,
                    R720XDipmicredentials.username,
                    R720XDipmicredentials.password)
    globals()[sys.argv[1]]()
