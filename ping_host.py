import subprocess

def ping_host(host):
    command = ['ping', '-c', '1', host]
    return subprocess.call(command) == 0
