import pyipmi
import pyipmi.interfaces
import logging

# IPMI raw-command(lun=0x00, cmd, raw_bytes)
# 3rd raw byte 0x00 for manual fan control, 0x01 for automatic
# 4th raw byte for speed:
# 0x02 ~ 1400
# 0x04 ~ 1600
# 0x06 ~ 1800
# 0x08 ~ 2000
# 0x0A ~ 2400
# 0x0C ~ 2600
# 0x0E ~ 2800
# 0x10 ~ 3000
# 0x12 ~ 3200
# 0x14 ~ 3400
# 0x16 ~ 3600
# 0x18 ~ 3800
# 0x1A ~ 4000
# 0x1C ~ 4200
# 0x1E ~ 4400
# 0x20 ~ 4600
# 0x22 ~ 4800
# 0x24 ~ 5200
# 0x26 ~ 5400
# 0x28 ~ 5600 RPM

class Server:
    def __init__(self, idrac_addr, hv_addr, username, password):
        self.idrac_addr = idrac_addr
        self.hv_addr = hv_addr
        self.username = username
        self.password = password
        self.interface = pyipmi.interfaces.create_interface('ipmitool', interface_type='lanplus')
        self.ipmi = pyipmi.create_connection(self.interface)
        self.ipmi.session.set_session_type_rmcp(host=self.idrac_addr)
        self.ipmi.session.set_auth_type_user(username=self.username, password=self.password)
        self.ipmi.session.establish()
        self.ipmi.target = pyipmi.Target(0x20)
        logging.info("Succesfully established IPMI-connection")

    def boot_server(self):
        self.ipmi.chassis_control_power_up()
        logging.info("Booting server")

    def shutdown_server(self):
        logging.info("Shutting down server")
        self.ipmi.chassis_control_soft_shutdown()

    def set_manual_fan_control(self):
        self.ipmi.raw_command(0x00, 0x30, [0x30, 0x01, 0x00])
        logging.info("Manual fan control enabled")

    def set_automatic_fan_control(self):
        self.ipmi.raw_command(0x00, 0x30, [0x30, 0x01, 0x01])
        logging.info("Automatic fan control enabled")

    def set_fan_speed(self, speed):
        self.ipmi.raw_command(0x00, 0x30, [0x30, 0x02, 0xff, speed])
        logging.info("Set fan speed to " + str(speed))

    def get_fan_speed(self, fan):
        if fan not in range(0, 6):
            raise ValueError
        # Fan sensor IDs are 0x30-0x35
        sensorvalue = self.ipmi.get_sensor_reading(0x30 + fan)
        # Raw value is 1/120 of real RPM value
        return sensorvalue[0] * 120

    def get_CPU_temperature(self, CPU):
        if CPU not in range(0, 2):
            raise ValueError
        # CPU temperature sensor IDs are 0xe & 0xf
        sensorvalue = self.ipmi.get_sensor_reading(0xe + CPU)
        # Temperature raw values are signed, but get_sensor_reading returns unsigned value
        return sensorvalue[0] - 128

    def get_inlet_temperature(self):
        sensorvalue = self.ipmi.get_sensor_reading(0x4)
        # Temperature raw values are signed, but get_sensor_reading returns unsigned value
        return sensorvalue[0] - 128

    def get_exhaust_temperature(self):
        sensorvalue = self.ipmi.get_sensor_reading(0x1)
        # Temperature raw values are signed, but get_sensor_reading returns unsigned value
        return sensorvalue[0] - 128

